angular.module('starter.controllers', [])

.controller('CalcCtrl', function($scope) {
	$scope.tip = 20;
	$scope.split = 4;
	
	$scope.tipon = "Total";
	var now = new Date();
	$scope.currentTime = now.getTime();
	
	$scope.$on("SetTipOn", function(ev, tipOn) {
		$scope.tipon = tipOn;
		var now2 = new Date();
		$scope.currentTime = now2.getTime();
	});
	
	//var admobParam= new admob.Params();
	//admobParam.isTesting=true;
	//admob.showBanner(admob.BannerSize.BANNER,admob.Position.TOP_CENTER,admobParam);
})

.controller('SettingsCtrl', function($scope) {
	$scope.tipon = "Total";
	
	$scope.SetTipOn = function(tipon) {
		$scope.$emit("TipOnChanged", tipon);
	};
})
